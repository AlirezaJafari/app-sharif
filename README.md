# Expense Request Management

The Sharif App provides comprehensive functionality for managing expense requests, facilitating efficient handling of financial transactions within the organization.

## Features

### Expense Request Submission
Users can submit expense requests through the Sharif App, providing detailed information about the incurred expenses. Key features include:

- **Expense Category Selection**: Users can choose from predefined expense categories (e.g., transportation, equipment purchase) to categorize their expenses accurately.
- **Description**: Users can provide a description of the expenses, including any relevant details or justifications.
- **Amount**: The requested amount for the expense is specified.
- **Supporting Documents**: Users have the option to attach supporting documents (e.g., receipts, invoices) to their expense requests for validation.
- **Bank Account Information**: Users can enter their bank account information, including IBAN and national ID (if required), to facilitate payment processing.

### Expense Request Review and Approval
Authorized personnel can review submitted expense requests, approve or reject them, and provide comments as necessary. Key features include:

- **Review Dashboard**: Authorized users have access to a dashboard where they can view all pending expense requests, along with their details.
- **Approval/Rejection**: Authorized users can approve or reject expense requests based on compliance with organizational policies and available budget.
- **Comments**: Users can provide comments or additional information when approving or rejecting expense requests to communicate with the requester effectively.

### Payment Processing
Once expense requests are approved, the Sharif App facilitates payment processing, ensuring timely and accurate disbursement of funds. Key features include:

- **Automatic Payment Scheduling**: Approved expense requests are processed for payment automatically based on predefined schedules, ensuring timely payments.
- **Manual Payment Option**: In cases where manual intervention is required (e.g., special approvals, exceptional circumstances), authorized personnel can initiate payments manually.
- **Integration with Bank APIs**: The Sharif App integrates with bank APIs to securely and efficiently transfer funds to the designated bank accounts of the requesters.
- **Error Handling and Logging**: Robust error handling mechanisms are in place to address any issues encountered during payment processing. Detailed logs are maintained to track the status of payments and troubleshoot any issues that may arise.

### Notifications
Users receive notifications via email or SMS regarding the status of their expense requests, keeping them informed throughout the process. Key features include:

- **Email Notifications**: Users receive email notifications when their expense requests are submitted, approved, or rejected.
- **SMS Alerts**: Users can opt to receive SMS alerts for important updates related to their expense requests, ensuring timely communication.

By leveraging the comprehensive expense request management features of the Sharif App, organizations can streamline their financial processes, enhance transparency, and ensure compliance with policies and regulations.
# Scheduled Payments Processing

The scheduled payments processing feature in Dadehpardaz ensures timely and reliable processing of payments based on predefined schedules. This functionality is implemented using Laravel Jobs and Commands.

## Job: `ProcessScheduledPayment`

The `ProcessScheduledPayment` job class executes the logic for processing scheduled payments. It triggers the `autoSchedulePayments()` method from the `AutoSchedulePayments` service. Any exceptions encountered during the process are logged using Laravel's logging mechanism.

## Command: `ProcessScheduledPayments`

The `ProcessScheduledPayments` command-line interface initiates the processing of scheduled payments. It defines the command signature as `payments:process-scheduled` and processes payments synchronously. Similar to the job class, any exceptions encountered during the process are logged for error tracking and debugging.

### Usage

To utilize the scheduled payments processing functionality:

1. **Schedule the Command**: Use Laravel's task scheduling feature (e.g., via `schedule:run` in cron) to execute the `payments:process-scheduled` command at desired intervals. An example cron entry is provided below:

    ```bash
    * * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
    ```

2. **Monitor Logs**: Keep track of application logs (configured in `config/logging.php`) to monitor scheduled payments processing and identify any errors or issues.

By leveraging Laravel's Jobs and Commands, Dadehpardaz ensures efficient and accurate processing of scheduled payments, contributing to seamless financial operations within the application.

## Packages Used

Dadehpardaz utilizes the following packages:

- **Guzzle**: Facilitates making HTTP requests.
- **Laravel Sanctum**: Provides a simple API authentication system.
- **Laravel Modules**: Enables modularization of the Laravel application.
- **L5 Repository**: Offers a simplified and consistent repository pattern for Laravel.

To integrate these packages into your project, use `composer require` followed by the package name.

## Installation

Follow these steps to install and set up Dadehpardaz:

1. **Clone the Repository**: Clone the repository to your local machine:

    ```bash
    git clone https://gitlab.com/AlirezaJafari/paystar-test.git
    ```

2. **Navigate to the Project Directory**: 

    ```bash
    cd app-test
    ```

3. **Install PHP Dependencies**: 

    ```bash
    composer install
    ```

4. **Configure Environment Variables**: Copy the `.env.example` file to `.env` and configure your environment variables:

    ```bash
    cp .env.example .env
    ```

5. **Generate Application Key**: 

    ```bash
    php artisan key:generate
    ```

6. **Run Migrations**: Execute database migrations to create necessary tables:

    ```bash
    php artisan migrate
    ```

7. **Seed the Database** (Optional): Populate the database with sample data:

    ```bash
    php artisan db:seed
    ```

8. **Start the Development Server**: 

    ```bash
    php artisan serve
    ```

9. **Access the Application**: Visit [http://localhost:8000](http://localhost:8000) in your web browser to access the application.

## Usage

Key Features of Dadehpardaz include:

- **User Authentication**: Registration, login, and authentication functionalities enable users to access the application securely.
- **Expense Request Submission**: Users can submit expense requests, providing details such as category, description, amount, and supporting documents.
- **Expense Request Review**: Authorized personnel can review submitted expense requests, approve or reject them, and provide comments as necessary.
- **Payment Processing**: Approved expense requests are processed for payment using integrated payment gateways, either automatically or manually.
- **Notifications**: Users receive notifications via email or SMS regarding the status of their expense requests.
- **Bank Integration**: The application integrates with bank APIs to process payments securely and efficiently.

By following these instructions and leveraging the outlined features, users can effectively manage tasks, orders, and financial transactions within Dadehpardaz.
