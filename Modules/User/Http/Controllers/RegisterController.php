<?php

namespace Modules\User\Http\Controllers;

use Modules\User\Http\Requests\RegisterUserRequest;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;

class RegisterController
{
    public function register(RegisterUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'national_code'=> $request->national_code,
            'password' => Hash::make($request->password),
        ]);

    $token = $user->createToken('API Token')->plainTextToken;

        return response()->json([
            'message' => 'User registered successfully',
            'user' => $user,
            'token' => $token,

        ]);

    }
}
