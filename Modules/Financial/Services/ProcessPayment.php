<?php

namespace Modules\Financial\Services;
use Exception;

class ProcessPayment
{
    private $bankApis = [
        '11' => 'type1',
        '22' => 'type2',
        '33' => 'type3'
    ];

    public function processPayment(string $iban, float $amount)
    {
        // Mock bank selection based on the first two digits of IBAN
        $bankCode = substr($iban, 0, 2);

        // Validate that the bank code exists in the system
        if (!$this->isValidBankCode($bankCode)) {
            throw new Exception('Invalid bank code: ' . $bankCode);
        }

        // Determine the bank API type based on the bank code
        $bankApiType = $this->getBankType($bankCode);

        // Call bank API to process payment
        $response = $this->callBankApi($this->bankApis[$bankApiType], $iban, $amount);

        // Check the response status
        if ($response['status'] !== 'success') {
            throw new Exception('Payment failed: ' . $response['message']);
        }

        // Payment successful
        return true;
    }

    private function isValidBankCode(string $bankCode)
    {
        // Logic to check if the bank code exists in the system
        // This can involve querying a database or other data source
        // For demonstration purposes, we assume that bank codes "11", "22", and "33" are valid
        return isset($this->bankApis[$bankCode]);
    }

    private function getBankType(string $bankCode)
    {
        // Return the bank API type for the given bank code
        return $this->bankApis[$bankCode];
    }

    private function callBankApi(string $bankApi, string $iban, float $amount)
    {
        // Mock implementation of calling the bank API
        // In a real-world scenario, you would call the respective API based on $bankApi parameter
        // Here, we simply return a mock response indicating success
        return ['status' => 'success', 'message' => 'Payment processed successfully by ' . $bankApi];
    }

}
