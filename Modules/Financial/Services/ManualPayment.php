<?php

namespace Modules\Financial\Services;

use Exception;
use Illuminate\Support\Facades\Log;
use Modules\Financial\Entities\ExpenseRequest;
use Modules\User\Entities\User;

class ManualPayment
{
    protected $processPayment;
    public function __construct(ProcessPayment $processPayment)
    {
        $this->processPayment =$processPayment;
    }
    public function manualPayment($userId, $expenseRequestIds)
    {
        // Load user and expense requests
        $user = User::findOrFail($userId);
        $expenseRequests = ExpenseRequest::whereIn('id', $expenseRequestIds)->get();


        // Perform manual payment operations
        foreach ($expenseRequests as $expenseRequest) {
            // Validate if the expense request belongs to the user
            if ($expenseRequest->user_id != $userId) {
                throw new Exception('Expense request does not belong to the user.');
            }
            try {
                // Process payment for each request
                $this->processPayment->processPayment($expenseRequest->iban, $expenseRequest->amount);
                // Update request status to paid
                $expenseRequest->update(['status' => 'paid']);
            } catch (Exception $e) {
                // Handle payment failure
                Log::error('Error processing scheduled payments: ' . $e->getMessage());
            }
        }

        // Optionally, you can perform additional actions here, such as sending notifications

        return true;
    }


}
