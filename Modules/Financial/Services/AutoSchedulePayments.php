<?php

namespace Modules\Financial\Services;

use Exception;
use Illuminate\Support\Facades\Log;
use Modules\Financial\Entities\ExpenseRequest;
use Modules\Financial\Services\ProcessPayment;

class AutoSchedulePayments
{
    protected $processPayment;
    public function __construct(ProcessPayment $processPayment)
    {
        $this->processPayment =$processPayment;
    }
    public function autoSchedulePayments()
    {
        // Logic to schedule payments automatically
        // Retrieve expense requests to be paid
        $requestsToPay = ExpenseRequest::where('status', 'approved')->get();

        foreach ($requestsToPay as $request) {
            try {
                // Call bank API to process payment for each request
                $this->processPayment->processPayment($request->iban, $request->amount);
                // Update request status to paid
                $request->update(['status' => 'paid']);
            } catch (Exception $e) {
                // Handle payment failure
                Log::error('Error processing scheduled payments: ' . $e->getMessage());
            }
        }
    }


}
