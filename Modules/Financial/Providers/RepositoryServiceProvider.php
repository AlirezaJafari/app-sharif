<?php

namespace Modules\Financial\Providers;

use Illuminate\Support\ServiceProvider;;

use Modules\Financial\Repositories\ExpenseRepository;
use Modules\Financial\Repositories\ExpenseRepositoryEloquent;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ExpenseRepository::class, ExpenseRepositoryEloquent::class);
    }
}
