<?php

namespace Modules\Financial\Entities;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{   protected $fillable = ['name']; // Assuming you have a 'name' field in the expense_categories table

    public function expenseRequests()
    {
        return $this->hasMany(ExpenseRequest::class);
    }
}
