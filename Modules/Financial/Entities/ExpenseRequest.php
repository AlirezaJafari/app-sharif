<?php

namespace Modules\Financial\Entities;

use Illuminate\Database\Eloquent\Model;

class ExpenseRequest extends Model
{
    protected $fillable = [
        'user_id',
        'expense_category_id',
        'description',
        'amount',
        'file',
        'iban',
        'national_code',
        'status',
        'reason',
    ];
    public function category()
    {
        return $this->belongsTo(ExpenseCategory::class);
    }
}
