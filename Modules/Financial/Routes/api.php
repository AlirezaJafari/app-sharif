<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Financial\Http\Controllers\ExpenseRequestController;
use Modules\Financial\Http\Controllers\ManualPaymentController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/fainancial', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:sanctum')->group(function () {
    Route::post('/submit-request', [ExpenseRequestController::class, 'submitRequest']);
    Route::get('/requests', [ExpenseRequestController::class, 'getRequests']);
    Route::put('/approve-request/{id}', [ExpenseRequestController::class, 'approveRequest']);
    Route::put('/reject-request/{id}', [ExpenseRequestController::class, 'rejectRequest']);

    Route::post('payments/manual', [ManualPaymentController::class, 'manualPayment']);
});
