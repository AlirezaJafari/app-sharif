<?php

namespace Modules\Financial\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository.
 *
 * @package namespace App\Repositories;
 */
interface ExpenseRepository extends RepositoryInterface
{
    public function create(array $data);

    public function getByUserId($userId);

    public function findByNationalCode($nationalCode);
}
