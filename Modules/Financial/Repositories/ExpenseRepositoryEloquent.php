<?php

namespace Modules\Financial\Repositories;

use Modules\Financial\Entities\ExpenseRequest;
use Modules\User\Entities\User;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class OrderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ExpenseRepositoryEloquent extends BaseRepository implements ExpenseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExpenseRequest::class;
    }

    public function create(array $data)
    {
        return ExpenseRequest::create($data);
    }

    public function getByUserId($userId)
    {
        return ExpenseRequest::where('user_id', $userId)->get();
    }

    /**
     * Find users by national code.
     *
     * @param string $nationalCode
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByNationalCode($nationalCode)
    {
        return User::where('national_code', $nationalCode)->get();
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
