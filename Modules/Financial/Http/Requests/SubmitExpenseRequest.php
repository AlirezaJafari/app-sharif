<?php

namespace Modules\Financial\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitExpenseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'expense_category_id' => 'required',
            'description' => 'required',
            'amount' => 'required|numeric',
            'file' => 'nullable|file',
            'iban' => 'required',
            'national_code' => 'required',
        ];
    }
}
