<?php

namespace Modules\Financial\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RejectExpenseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'request_ids' => 'required|array',
            'request_ids.*' => 'exists:expense_requests,id',
            'reason' => 'required|string',
        ];
    }
}
