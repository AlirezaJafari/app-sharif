<?php

namespace Modules\Financial\Http\Controllers;
use App\Http\Controllers\Controller;
use Modules\Financial\Http\Requests\ApproveExpenseRequest;
use Modules\Financial\Http\Requests\RejectExpenseRequest;
use Modules\Financial\Http\Requests\SubmitExpenseRequest;
use Modules\Financial\Repositories\ExpenseRepository;

use Modules\Financial\Http\Resources\ExpenseRequestResource;
//use Modules\Financial\Notifications\RequestRejectedNotification;

class ExpenseRequestController extends Controller
{
    protected $expenseRequestRepository;

    public function __construct(ExpenseRepository $expenseRequestRepository)
    {
        $this->expenseRequestRepository = $expenseRequestRepository;
    }

    public function submitRequest(SubmitExpenseRequest $request)
    {
        $validatedData = $request->validated();

        // Process file upload
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->store('uploads'); // Store file in the 'uploads' directory
            $validatedData['file'] = $path; // Save file path in the database
        }

        $validatedData['user_id'] = auth()->id();

        $expenseRequest = $this->expenseRequestRepository->create($validatedData);

        return new ExpenseRequestResource($expenseRequest);
    }

    public function getRequests()
    {
        $requests = $this->expenseRequestRepository->getByUserId(auth()->id());

        return ExpenseRequestResource::collection($requests);
    }

    public function approveRequest(ApproveExpenseRequest $request)
    {
        $validatedData = $request->validated();
        $requestIds = $validatedData['request_ids'];

        foreach ($requestIds as $requestId) {
            $expenseRequest = $this->expenseRequestRepository->find($requestId);
            $expenseRequest->update(['status' => 'approved']);
        }

        return response()->json(['message' => 'Requests approved successfully']);
    }

    public function rejectRequest(RejectExpenseRequest $request)
    {
        $validatedData = $request->validated();
        $requestIds = $validatedData['request_ids'];
        $reason = $validatedData['reason'];

        foreach ($requestIds as $requestId) {
            $expenseRequest = $this->expenseRequestRepository->find($requestId);
            $expenseRequest->update(['status' => 'rejected', 'reason' => $reason]);
        }
        // Notify user about rejection
        // auth()->user()->notify(new RequestRejectedNotification());

        return response()->json(['message' => 'Requests rejected successfully']);
    }
}
