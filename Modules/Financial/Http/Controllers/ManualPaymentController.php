<?php

namespace Modules\Financial\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Financial\Services\ManualPayment;

class ManualPaymentController
{
    protected $paymentService;

    public function __construct(ManualPayment $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function manualPayment(Request $request)
    {
        // Validate the request data (e.g., user ID, expense request IDs, etc.)
        $request->validate([
            'user_id' => 'required',
            'expense_request_ids' => 'required|array',
        ]);

        // Get user ID and expense request IDs from the request
        $userId = $request->user_id;
        $expenseRequestIds = $request->expense_request_ids;

        // Process manual payment
        try {
            $this->paymentService->manualPayment($userId, $expenseRequestIds);
            return response()->json(['message' => 'Manual payment processed successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

}
