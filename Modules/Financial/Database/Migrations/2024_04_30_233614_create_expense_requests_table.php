<?php

// database/migrations/YYYY_MM_DD_create_expense_requests_table.php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('expense_category_id');
            $table->text('description');
            $table->decimal('amount', 10, 2);
            $table->string('file')->nullable();
            $table->string('iban');
            $table->string('national_code');
            $table->enum('status', ['pending', 'approved', 'rejected','paid'])->default('pending');
            $table->text('reason')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // Define foreign key constraint for expense_category_id if applicable
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_requests');
    }
}
