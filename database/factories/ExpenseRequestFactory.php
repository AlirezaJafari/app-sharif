<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;
use Modules\Financial\Entities\ExpenseCategory;
use Modules\Financial\Entities\ExpenseRequest;
use Modules\User\Entities\User;

$factory->define(ExpenseRequest::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'expense_category_id' => function () {
            // Assuming you have an ExpenseCategory model
            return factory(ExpenseCategory::class)->create()->id;
        },
        'description' => $faker->sentence,
        'amount' => $faker->randomFloat(2, 10, 100),
        'file' => null, // You can add file generation logic if needed
        'iban' => $faker->iban('IR'),
        'national_code' => $faker->unique()->numberBetween(1000000000, 9999999999),
        'status' => $faker->randomElement(['pending', 'approved', 'rejected']),
        'reason' => $faker->optional()->sentence,
    ];
});
