<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Modules\User\Entities\User;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'national_code' => $faker->unique()->numberBetween(1000000000, 9999999999),
        'password' => bcrypt('password'), // Default password
        'remember_token' => Str::random(10),
    ];
});


