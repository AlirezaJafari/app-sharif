<?php

use Illuminate\Database\Seeder;
use Modules\Financial\Entities\ExpenseCategory;
use Modules\Financial\Entities\ExpenseRequest;
use Modules\User\Entities\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(ExpenseCategory::class, 10)->create();
        factory(ExpenseRequest::class, 10)->create();
        factory(User::class, 20)->create();
        // $this->call(UserSeeder::class);
    }
}
