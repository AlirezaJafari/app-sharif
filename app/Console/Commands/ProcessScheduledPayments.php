<?php

namespace App\Console\Commands;

use App\Jobs\ProcessScheduledPayment;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\Financial\Services\AutoSchedulePayments;

class ProcessScheduledPayments extends Command
{
    protected $signature = 'payments:process-scheduled';

    protected $description = 'Process scheduled payments synchronously';

    public function handle()
    {
        try {
            // Instantiate the AutoSchedulePayments service within the handle method
            $autoSchedulePayments = app(AutoSchedulePayments::class);
            $autoSchedulePayments->autoSchedulePayments()->dailyAt('10:00');
        } catch (\Exception $e) {
            Log::error('Error processing scheduled payments: ' . $e->getMessage());
        }
    }

}

