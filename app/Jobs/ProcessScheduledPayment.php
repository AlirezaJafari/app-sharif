<?php

namespace App\Jobs;

use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Modules\Financial\Services\AutoSchedulePayments;

class ProcessScheduledPayment
{
    use Dispatchable;

    protected $autoSchedulePayments;

    public function __construct(AutoSchedulePayments $autoSchedulePayments)
    {
        $this->autoSchedulePayments = $autoSchedulePayments;
    }

    public function handle()
    {
        try {
            $this->autoSchedulePayments->autoSchedulePayments();
        } catch (\Exception $e) {
            Log::error('Error processing scheduled payments: ' . $e->getMessage());
        }
    }
}
