<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

class RequestRejectedNotification extends Notification
{
    use Queueable;

    public function via($notifiable)
    {
        return ['mail', 'nexmo']; // Send notification via email and SMS (Nexmo)
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Your request has been rejected.') // Email body
            ->line('Please contact support for more information.');
    }

    public function toNexmo($notifiable)
    {
//        return (new NexmoMessage)
//            ->content('Your request has been rejected. Please contact support for more information.'); // SMS content
    }
}
